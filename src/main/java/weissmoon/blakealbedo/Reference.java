package weissmoon.blakealbedo;

/**
 * Created by Weissmoon on 11/27/17.
 */
public class Reference {
    public static final String MOD_ID = "blakealbedo";
    public static final String MOD_NAME = "BlakeAlbedo";
    public static final String VERSION = "0.0.4";
    public static final String AUTHOR = "Weissmoon";
    public static final String GUI_FACTORY_CLASS = "weissmoon.blakealbedo.gui.GuiFactory";
    public static final String DESCRIPTION = "Mod Albedo.";
}
