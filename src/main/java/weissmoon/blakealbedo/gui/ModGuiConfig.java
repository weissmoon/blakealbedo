package weissmoon.blakealbedo.gui;

import net.minecraft.client.gui.GuiScreen;
import net.minecraftforge.common.config.ConfigCategory;
import net.minecraftforge.common.config.ConfigElement;
import net.minecraftforge.fml.client.config.GuiConfig;
import net.minecraftforge.fml.client.config.IConfigElement;
import weissmoon.blakealbedo.ConfigurationHandler;
import weissmoon.blakealbedo.Reference;

import java.util.ArrayList;
import java.util.List;

public class ModGuiConfig extends GuiConfig{
    public ModGuiConfig (GuiScreen guiScreen){
        super(guiScreen, getConfigElements(),
                //new ConfigElement(ConfigurationHandler.configuration.getCategory("general")).getChildElements(),
                Reference.MOD_ID,
                false,
                false,
                GuiConfig.getAbridgedConfigPath(ConfigurationHandler.configuration.toString()));
    }


    public static List<IConfigElement> getConfigElements(){
        List<IConfigElement> list = new ArrayList<>();

        //list.add(new ConfigElement(ConfigurationHandler.configuration.getCategory(ConfigurationHandler.CATEGORY_MODS)));
        for(String cat : ConfigurationHandler.categories){
            IConfigElement e = new ConfigElement(ConfigurationHandler.configuration.getCategory(cat));
            list.add(e);
        }

        return list;
    }
}
