package weissmoon.blakealbedo.api;

import elucent.albedo.lighting.Light;
import net.minecraft.tileentity.TileEntity;

/**
 * Created by Weissmoon on 5/9/18.
 */
public interface ITileColor<TILE extends TileEntity> {

    public boolean doesColor(TILE entity);

    public Light getLight(TILE entity);

    public Class<TILE> getTileClass();
}
