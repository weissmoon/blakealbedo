package weissmoon.blakealbedo.api;

import elucent.albedo.lighting.Light;
import net.minecraft.entity.Entity;

/**
 * Created by Weissmoon on 2/10/19.
 */
public interface IEntityColor<ENTITY extends Entity> {

    public boolean doesColor(ENTITY entity);

    public Light getLight(ENTITY entity);

    public Class<ENTITY> getEntityClass();
}
