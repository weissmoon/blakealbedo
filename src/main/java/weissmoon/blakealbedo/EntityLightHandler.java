package weissmoon.blakealbedo;

import elucent.albedo.event.RenderEntityEvent;
import elucent.albedo.lighting.LightManager;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.Entity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import weissmoon.blakealbedo.api.IEntityColor;

import java.util.HashMap;

/**
 * Created by Weissmoon on 2/10/19.
 */
public class EntityLightHandler {

    private HashMap<Class <? extends TileEntity >, IEntityColor> entityList = new HashMap<Class <? extends TileEntity >, IEntityColor>();
    @SubscribeEvent
    public void onEntityRender(RenderEntityEvent event) {
        World world = Minecraft.getMinecraft().world;
        if (world == null) return;
        Entity e = event.getEntity();

        IEntityColor iEntityColor = entityList.get(e.getClass());

        if (iEntityColor != null) {
            if (iEntityColor.doesColor(e)) {
                LightManager.addLight(iEntityColor.getLight(e));
            }
        }

    }

    public void registerEntityColor(IEntityColor entityColor){
        entityList.put(entityColor.getEntityClass(), entityColor);
    }

    void wipeTileList(){
        this.entityList = new HashMap<Class <? extends TileEntity >, IEntityColor>();
    }
}
