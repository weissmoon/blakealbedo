package weissmoon.blakealbedo;

import elucent.albedo.event.ProfilerStartEvent;
import elucent.albedo.lighting.LightManager;
import net.minecraft.client.Minecraft;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import weissmoon.blakealbedo.api.ITileColor;

import java.util.HashMap;

/**
 * Created by Weissmoon on 7/13/18.
 */
public class TileLightHandler {

    private HashMap<Class <? extends TileEntity >, ITileColor> tileList = new HashMap<Class <? extends TileEntity >, ITileColor>();
    @SubscribeEvent
    public void onProfileStart(ProfilerStartEvent evnt) {
        if (evnt.getSection().compareTo("terrain") == 0) {
            World world = Minecraft.getMinecraft().world;
            if (world == null) return;

            for (TileEntity t : world.loadedTileEntityList) {
                ITileColor iTileColor = tileList.get(t.getClass());

                if (iTileColor != null){
                    if(iTileColor.doesColor(t)){
                        LightManager.addLight(iTileColor.getLight(t));
                    }


//                    ItemStack stack = ((TileIncensePlate)t).getItemHandler().getStackInSlot(0);
//                    if(!stack.isEmpty() && ((TileIncensePlate)t).burning) {
//                        Brew brew = ((ItemIncenseStick) ModItems.incenseStick).getBrew(stack);
//                        Color color = new Color(brew.getColor(stack));
//
//                        LightManager.addLight(Light.builder().
//                                color(color.getRed()/200f, color.getGreen()/200f, color.getBlue()/200f, 1.3F).
//                                pos(t.getPos()).
//                                radius((float)
//                                        (3F + Math.abs(
//                                                ((Math.sin(System.currentTimeMillis() / 100))+ 10 ) / 20) +
//                                                (((Math.sin(System.currentTimeMillis() / 73.156841)) + 19.5) / 20))).
//                                //radius(rand.nextFloat() * 1.8F).
//                                        build());
//                    }
                }
            }
        }
    }

    public void registerTileColor(ITileColor tileColor){
        tileList.put(tileColor.getTileClass(), tileColor);
    }

    void wipeTileList(){
        this.tileList = new HashMap<Class <? extends TileEntity >, ITileColor>();
    }
}
