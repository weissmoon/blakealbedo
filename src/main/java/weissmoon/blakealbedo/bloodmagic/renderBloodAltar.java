package weissmoon.blakealbedo.bloodmagic;

import elucent.albedo.lighting.Light;
import net.minecraft.tileentity.TileEntityBeacon;
import weissmoon.blakealbedo.api.ITileColor;

/**
 * Created by Weissmoon on 5/9/18.
 */
public class renderBloodAltar implements ITileColor<TileEntityBeacon> {

    @Override
    public boolean doesColor(TileEntityBeacon entity) {
        return false;
    }

    @Override
    public Light getLight(TileEntityBeacon entity) {
        return null;
    }

    @Override
    public Class getTileClass() {
        return null;
    }
}
