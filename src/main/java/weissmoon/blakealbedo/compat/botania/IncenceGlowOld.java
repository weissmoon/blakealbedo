package weissmoon.blakealbedo.compat.botania;

import elucent.albedo.event.ProfilerStartEvent;
import elucent.albedo.lighting.Light;
import elucent.albedo.lighting.LightManager;
import io.netty.util.internal.MathUtil;
import net.minecraft.client.Minecraft;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import vazkii.botania.api.brew.Brew;
import vazkii.botania.common.block.tile.TileIncensePlate;
import vazkii.botania.common.item.ModItems;
import vazkii.botania.common.item.brew.ItemIncenseStick;

import java.awt.*;
import java.util.Random;

/**
 * Created by Weissmoon on 6/21/18.
 */
public class IncenceGlowOld {

    private Random rand = new Random();
    @SubscribeEvent
    public void onProfileStart(ProfilerStartEvent evnt) {
        if (evnt.getSection().compareTo("terrain") == 0) {
            World world = Minecraft.getMinecraft().world;
            if (world == null) return;

            for (TileEntity t : world.loadedTileEntityList) {
                if (t instanceof TileIncensePlate){
                    ItemStack stack = ((TileIncensePlate)t).getItemHandler().getStackInSlot(0);
                    if(!stack.isEmpty() && ((TileIncensePlate)t).burning) {
                        Brew brew = ((ItemIncenseStick) ModItems.incenseStick).getBrew(stack);
                        Color color = new Color(brew.getColor(stack));

                        LightManager.addLight(Light.builder().
                                color(color.getRed()/200f, color.getGreen()/200f, color.getBlue()/200f, 1.3F).
                                pos(t.getPos()).
                                radius((float)
                                        (3F + Math.abs(
                                                ((Math.sin(System.currentTimeMillis() / 100))+ 10 ) / 20) +
                                                (((Math.sin(System.currentTimeMillis() / 73.156841)) + 19.5) / 20))).
                                //radius(rand.nextFloat() * 1.8F).
                                build());
                    }
                }
            }
        }
    }
}
