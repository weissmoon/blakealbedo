package weissmoon.blakealbedo.compat.botania;

import net.minecraftforge.common.config.Configuration;
import net.minecraftforge.fml.common.Loader;
import net.minecraftforge.fml.common.LoaderState;
import net.minecraftforge.fml.common.event.*;
import weissmoon.blakealbedo.BlakeAlbedo;
import weissmoon.blakealbedo.ConfigurationHandler;
import weissmoon.blakealbedo.compat.CompatModule;

/**
 * Created by Weissmoon on 6/20/18.
 */
public class BotaniaCompat implements CompatModule {

    private IncenceGlowOld incenceGlowOld;
    private IncenseGlow incenceGlow;
    private boolean incenceGlowC;

    @Override
    public void e(FMLPreInitializationEvent event) {

    }

    @Override
    public void e(FMLInitializationEvent event) {
        //incenceGlowOld = new IncenceGlowOld();
        if(incenceGlowC) {
            incenceGlow = new IncenseGlow();
            BlakeAlbedo.instance.getTileLightHandler().registerTileColor(incenceGlow);
        }
        //MinecraftForge.EVENT_BUS.register(incenceGlowOld);
    }

    @Override
    public void e(FMLPostInitializationEvent event) {

    }

    @Override
    public void e(FMLServerAboutToStartEvent evt) {

    }

    @Override
    public void e(FMLServerStartingEvent evt) {

    }

    @Override
    public void e(FMLServerStartedEvent evt) {

    }

    @Override
    public void e(FMLServerStoppingEvent evt) {

    }

    @Override
    public void e(FMLServerStoppedEvent evt) {

    }

    @Override
    public void reconfigureOptions() {
        if(incenceGlowC) {
            incenceGlow = new IncenseGlow();
            BlakeAlbedo.instance.getTileLightHandler().registerTileColor(incenceGlow);
        }
    }

    @Override
    public void handleModuleConfig(Configuration configurationObj) {
        Configuration config = configurationObj;
        incenceGlowC = config.getBoolean("incensePlate", getModName(), true, "Incense Plate Glow");
        if (!(configurationObj.hasChanged()))
            ConfigurationHandler.categories.add(getModName());
    }

    @Override
    public boolean proccessState(LoaderState.ModState state) {
        switch(state){
            case INITIALIZED:
                return true;
        }
        return false;
    }

    @Override
    public String getModName() {
        return "botania";
    }

}
