package weissmoon.blakealbedo.compat.botania;

import elucent.albedo.lighting.Light;
import elucent.albedo.lighting.LightManager;
import net.minecraft.item.ItemStack;
import vazkii.botania.api.brew.Brew;
import vazkii.botania.common.block.tile.TileIncensePlate;
import vazkii.botania.common.item.ModItems;
import vazkii.botania.common.item.brew.ItemIncenseStick;
import weissmoon.blakealbedo.api.ITileColor;

import java.awt.*;

/**
 * Created by Weissmoon on 7/13/18.
 */
public class IncenseGlow implements ITileColor<TileIncensePlate> {
    @Override
    public boolean doesColor(TileIncensePlate entity) {

        ItemStack stack = entity.getItemHandler().getStackInSlot(0);
        return (!stack.isEmpty() && entity.burning);
    }

    @Override
    public Light getLight(TileIncensePlate entity) {
        ItemStack stack = entity.getItemHandler().getStackInSlot(0);
        Brew brew = ((ItemIncenseStick) ModItems.incenseStick).getBrew(stack);
        Color color = new Color(brew.getColor(stack));

        return Light.builder().
                color(color.getRed()/200f, color.getGreen()/200f, color.getBlue()/200f, 1.3F).
                pos(entity.getPos()).
                radius((float)
                        (3F + Math.abs(
                                ((Math.sin(System.currentTimeMillis() / 100))+ 10 ) / 20) +
                                (((Math.sin(System.currentTimeMillis() / 73.156841)) + 19.5) / 20))).
                build();
    }

    @Override
    public Class<TileIncensePlate> getTileClass() {
        return TileIncensePlate.class;
    }
}
