package weissmoon.blakealbedo.compat.torched;

import elucent.albedo.lighting.Light;
import me.ichun.mods.torched.common.entity.EntityTorchFirework;
import weissmoon.blakealbedo.api.IEntityColor;

/**
 * Created by Weissmoon on 2/10/19.
 */
public class TorchRocketGlow implements IEntityColor<EntityTorchFirework> {
    @Override
    public boolean doesColor(EntityTorchFirework entity) {
        return true;
    }

    @Override
    public Light getLight(EntityTorchFirework entity) {
        float scale = 1.0F + (entity.getRocket() ? 1.0F :((float)entity.getTorches() / 96F));


        return Light.builder().
                color(1, 1, 1, .8F).
                pos(entity.getPositionVector()).
                radius(6F + scale).
                build();
    }

    @Override
    public Class<EntityTorchFirework> getEntityClass() {
        return EntityTorchFirework.class;
    }
}
