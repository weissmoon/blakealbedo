package weissmoon.blakealbedo.compat.torched;

import elucent.albedo.lighting.Light;
import me.ichun.mods.torched.common.entity.EntityTorch;
import weissmoon.blakealbedo.api.IEntityColor;

/**
 * Created by Weissmoon on 2/10/19.
 */
public class TorchGlow implements IEntityColor<EntityTorch> {
    @Override
    public boolean doesColor(EntityTorch entity) {
        return true;
    }

    @Override
    public Light getLight(EntityTorch entity) {
        return Light.builder().
                color(1, 1, 1, .8F).
                pos(entity.getPositionVector()).
                radius(9F).
                build();
    }

    @Override
    public Class<EntityTorch> getEntityClass() {
        return EntityTorch.class;
    }
}
