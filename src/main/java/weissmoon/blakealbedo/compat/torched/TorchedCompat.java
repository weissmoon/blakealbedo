package weissmoon.blakealbedo.compat.torched;

import net.minecraftforge.common.config.Configuration;
import net.minecraftforge.fml.common.LoaderState;
import net.minecraftforge.fml.common.event.*;
import weissmoon.blakealbedo.BlakeAlbedo;
import weissmoon.blakealbedo.ConfigurationHandler;
import weissmoon.blakealbedo.compat.CompatModule;
import weissmoon.blakealbedo.compat.botania.IncenseGlow;

/**
 * Created by Weissmoon on 2/10/19.
 */
public class TorchedCompat implements CompatModule {

    private boolean torchC;
    private TorchGlow torchGlow;
    private boolean torchRocketC;
    private TorchRocketGlow torchRocketGlow;

    @Override
    public void e(FMLPreInitializationEvent event) {

    }

    @Override
    public void e(FMLInitializationEvent event) {
        if(torchC) {
            torchGlow = new TorchGlow();
            BlakeAlbedo.instance.getEntityLightHandler().registerEntityColor(torchGlow);
        }
        if(torchRocketC){
            torchRocketGlow = new TorchRocketGlow();
            BlakeAlbedo.instance.getEntityLightHandler().registerEntityColor(torchRocketGlow);
        }
    }

    @Override
    public void e(FMLPostInitializationEvent event) {

    }

    @Override
    public void e(FMLServerAboutToStartEvent evt) {

    }

    @Override
    public void e(FMLServerStartingEvent evt) {

    }

    @Override
    public void e(FMLServerStartedEvent evt) {

    }

    @Override
    public void e(FMLServerStoppingEvent evt) {

    }

    @Override
    public void e(FMLServerStoppedEvent evt) {

    }

    @Override
    public void reconfigureOptions() {
        if(torchC) {
            torchGlow = new TorchGlow();
            BlakeAlbedo.instance.getEntityLightHandler().registerEntityColor(torchGlow);
        }
        if(torchRocketC){
            torchRocketGlow = new TorchRocketGlow();
            BlakeAlbedo.instance.getEntityLightHandler().registerEntityColor(torchRocketGlow);
        }
    }

    @Override
    public void handleModuleConfig(Configuration configurationObj) {
        Configuration config = configurationObj;
        torchC = config.getBoolean("entityTorch", getModName(), true, "Torch Glow");
        torchRocketC = config.getBoolean("entityTorchRocket", getModName(), true, "Torch Rocket Glow");
        if (!(configurationObj.hasChanged()))
            ConfigurationHandler.categories.add(getModName());
    }

    @Override
    public boolean proccessState(LoaderState.ModState state) {
        switch(state){
            case INITIALIZED:
                return true;
        }
        return false;
    }

    @Override
    public String getModName() {
        return "torched";
    }

}
