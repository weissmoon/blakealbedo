package weissmoon.blakealbedo.compat;

import net.minecraftforge.common.config.Configuration;
import net.minecraftforge.fml.common.Loader;
import net.minecraftforge.fml.common.event.*;
import weissmoon.blakealbedo.BlakeAlbedo;
import weissmoon.blakealbedo.compat.botania.BotaniaCompat;
import weissmoon.blakealbedo.compat.inspirations.InspirationsCompat;
import weissmoon.blakealbedo.compat.torched.TorchedCompat;

import java.lang.reflect.Method;
import java.util.HashMap;

/**
 * Created by Weissmoon on 1/2/18.
 */
public class CompatModules {

    private HashMap<String, CompatModule> map = new HashMap<>(1);

    public void setupModules(){
        if(Loader.isModLoaded("botania")){
            map.put("botania", new BotaniaCompat());
        }
        if(Loader.isModLoaded("inspirations")){
            map.put("inspirations", new InspirationsCompat());
        }
        if(Loader.isModLoaded("torched")){
            map.put("torched", new TorchedCompat());
        }
        if(Loader.isModLoaded("hardcoredarkness")){
            //HardcoreDarknessCompat hardcoreDarknessCompat = new HardcoreDarknessCompat();
            //map.put("hardcoredarkness", hardcoreDarknessCompat);
        }
    }

    public void stateevent(FMLStateEvent event){
        for(String item: map.keySet()){
            CompatModule module = map.get(item);
            if(module.proccessState(event.getModState())) {
                try {
                    Method m = module.getClass().getMethod("e", event.getClass());
                    m.invoke(module, event);
                } catch (Exception e) {
                    BlakeAlbedo.instance.log.fatal(e);
                }
            }
        }
    }

    public void reconfigureModules(Configuration configuration){
        for(String item: map.keySet()){
            CompatModule module = map.get(item);
            try {
                module.reconfigureOptions();
            } catch (Exception e) {
                BlakeAlbedo.instance.log.fatal(e);
            }
        }
    }
    public void handleConfig(Configuration configuration){
        for(String item: map.keySet()){
            CompatModule module = map.get(item);
                try {
                    module.handleModuleConfig(configuration);
                } catch (Exception e) {
                    BlakeAlbedo.instance.log.fatal(e);
                }
        }
    }

    public CompatModule getModel(String name){
        if(map.containsKey(name))
            return map.get(name);
        return null;
    }

    public boolean isLoaded(String name){
        return map.containsKey(name);
    }
}
