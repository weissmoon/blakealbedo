package weissmoon.blakealbedo.compat;

import net.minecraftforge.common.config.Configuration;
import net.minecraftforge.fml.common.Loader;
import net.minecraftforge.fml.common.LoaderState;
import net.minecraftforge.fml.common.event.*;

/**
 * Created by Weissmoon on 12/30/17.
 */
public interface CompatModule {
//    public void preInit(FMLPreInitializationEvent event);
//    public void init(FMLInitializationEvent event);
//    public void postInit(FMLPostInitializationEvent event);
//    public void serverAbouttoStart(FMLServerAboutToStartEvent evt);
//    public void serverStarting(FMLServerStartingEvent evt);
//    public void serverStarted(FMLServerStartedEvent evt);
//    public void serverAbouttoStop(FMLServerStoppingEvent evt);
//    public void serverStoped(FMLServerStoppedEvent evt);

    public void e(FMLPreInitializationEvent event);
    public void e(FMLInitializationEvent event);
    public void e(FMLPostInitializationEvent event);
    public void e(FMLServerAboutToStartEvent evt);
    public void e(FMLServerStartingEvent evt);
    public void e(FMLServerStartedEvent evt);
    public void e(FMLServerStoppingEvent evt);
    public void e(FMLServerStoppedEvent evt);
    public void reconfigureOptions();
    public void handleModuleConfig(Configuration configurationObj);

    public boolean proccessState(LoaderState.ModState state);

    public String getModName();

    public default boolean processModule() {
        return Loader.isModLoaded(getModName());
    }
}
