package weissmoon.blakealbedo.compat.inspirations;

import knightminer.inspirations.Inspirations;
import knightminer.inspirations.building.block.BlockEnlightenedBush;
import net.minecraft.block.state.IBlockState;
import net.minecraft.util.ResourceLocation;

/**
 * Created by Weissmoon on 7/23/18.
 */
public class BlockEnlightenedBushLit extends BlockEnlightenedBush {

    BlockEnlightenedBushLit(){
        super();
        setRegistryName(new ResourceLocation(Inspirations.modID, "enlightened_bush"));
    }
    @Override
    public int getLightValue(IBlockState state)
    {
        return 0;
     //   return this.lightValue;
    }
}
