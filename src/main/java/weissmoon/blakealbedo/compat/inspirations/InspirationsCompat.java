package weissmoon.blakealbedo.compat.inspirations;

import knightminer.inspirations.building.InspirationsBuilding;
import knightminer.inspirations.common.Config;
import net.minecraft.block.Block;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.common.config.Configuration;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.fml.common.LoaderState;
import net.minecraftforge.fml.common.event.*;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import weissmoon.blakealbedo.BlakeAlbedo;
import weissmoon.blakealbedo.ConfigurationHandler;
import weissmoon.blakealbedo.compat.CompatModule;

/**
 * Created by Weissmoon on 7/23/18.
 */
public class InspirationsCompat implements CompatModule{

    private boolean bushGlowC;
    private boolean bushEnabled;
    private BlockEnlightenedBushGlow bushGlow;
    BlockEnlightenedBushLit bushLit;

    @Override
    public void e(FMLPreInitializationEvent event) {
        bushEnabled = Config.enableEnlightenedBush;
        if (bushEnabled)
            MinecraftForge.EVENT_BUS.register(this);
    }

    @SubscribeEvent
    public void registerBlocks(RegistryEvent.Register<Block> event) {
        bushLit = new BlockEnlightenedBushLit();
        InspirationsBuilding.enlightenedBush = bushLit;
        event.getRegistry().register(bushLit);
    }

    @Override
    public void e(FMLInitializationEvent event) {
        if(bushGlowC && bushEnabled) {
            bushGlow = new BlockEnlightenedBushGlow();
            BlakeAlbedo.instance.getTileLightHandler().registerTileColor(bushGlow);
        }
    }

    @Override
    public void e(FMLPostInitializationEvent event) {

    }

    @Override
    public void e(FMLServerAboutToStartEvent evt) {

    }

    @Override
    public void e(FMLServerStartingEvent evt) {

    }

    @Override
    public void e(FMLServerStartedEvent evt) {

    }

    @Override
    public void e(FMLServerStoppingEvent evt) {

    }

    @Override
    public void e(FMLServerStoppedEvent evt) {

    }

    @Override
    public void reconfigureOptions() {
        if(bushGlowC && bushEnabled) {
            bushGlow = new BlockEnlightenedBushGlow();
            BlakeAlbedo.instance.getTileLightHandler().registerTileColor(bushGlow);
        }
    }

    @Override
    public void handleModuleConfig(Configuration configurationObj) {
        Configuration config = configurationObj;
        bushGlowC = config.getBoolean("bushGlow", getModName(), true, "Enlightened Bush Light Coloring");
        if (!(configurationObj.hasChanged()))
            ConfigurationHandler.categories.add(getModName());
    }

    @Override
    public boolean proccessState(LoaderState.ModState state) {
        switch(state){
            case PREINITIALIZED:
            case INITIALIZED:
                return true;
        }
        return false;
    }

    @Override
    public String getModName() {
        return "inspirations";
    }
}
