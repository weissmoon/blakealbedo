package weissmoon.blakealbedo.compat.inspirations;

import elucent.albedo.lighting.Light;
import knightminer.inspirations.building.block.BlockEnlightenedBush;
import knightminer.inspirations.building.tileentity.TileEnlightenedBush;
import net.minecraft.block.state.IBlockState;
import weissmoon.blakealbedo.api.ITileColor;

/**
 * Created by Weissmoon on 7/23/18.
 */
public class BlockEnlightenedBushGlow implements ITileColor<TileEnlightenedBush> {
    @Override
    public boolean doesColor(TileEnlightenedBush entity) {
        return true;
    }

    @Override
    public Light getLight(TileEnlightenedBush entity) {
        IBlockState state = entity.getWorld().getBlockState(entity.getPos());
        BlockEnlightenedBush.LightsType lightsType = BlockEnlightenedBush.LightsType.fromMeta(state.getBlock().damageDropped(state));
        int color;
        switch(lightsType){
            case WHITE:
                return null;
            case RED:
            case BLUE:
            case GREEN:
                color = lightsType.getColor();
                break;
            case RAINBOW:
                double gs1 = (Math.sin((double)System.currentTimeMillis() / 10000) + 1) * 0.2F + 0.6F;
                float r1 =   (float) Math.abs(Math.sin(105 * gs1));
                float g1 =   (float) Math.abs(Math.sin(25  * gs1));
                float b1 =   (float) Math.abs(Math.sin(145 * gs1));
                return Light.builder().
                        pos(entity.getPos()).
                        color(r1, g1, b1).
                        radius(15).
                        build();
            case CHRISTMAS:
                float gs = (float) (Math.sin(System.currentTimeMillis() / 1000) + 1) * 0.2F + 0.6F;
                float r =  (float) Math.abs(Math.sin(105 * gs));
                float g =  (float) Math.abs(Math.sin(25  * gs));
                float b =  (float) Math.abs(Math.sin(145 * gs));
                return Light.builder().
                        pos(entity.getPos()).
                        color(r, g, b).
                        radius(15).
                        build();
                default:
                    return null;
        }
        return Light.builder().
                pos(entity.getPos()).
                color(color, false).
                radius(15).
                build();
    }

    @Override
    public Class<TileEnlightenedBush> getTileClass() {
        return TileEnlightenedBush.class;
    }
}
