package weissmoon.blakealbedo;

import com.google.common.collect.Lists;
import net.minecraftforge.common.config.Configuration;
import net.minecraftforge.fml.client.event.ConfigChangedEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import weissmoon.blakealbedo.compat.CompatModules;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

/**
 * Created by Weissmoon on 5/9/18.
 */
public class ConfigurationHandler {
    public static Configuration configuration;
    public static String CATEGORY_GENERAL = "general";
    public static List<String> categories = Lists.newArrayList();
    //public static String CATEGORY_MODS = "MODS";

    Pattern commaDelimitedPattern = Pattern.compile("([A-Za-z]+((,){1}( )*|$))+?");

    public static void initC (File configFile){
        if (configuration == null){
            configuration = new Configuration(configFile);
            loadCofiguration();
        }
    }

    @SubscribeEvent
    public void onConfigurationChangedEvent(ConfigChangedEvent.OnConfigChangedEvent event){
        if (event.getModID().equals(Reference.MOD_ID)){
            loadCofiguration();
        }
        BlakeAlbedo.instance.reconfigureOptions(configuration);
    }

    private static void loadCofiguration (){
        BlakeAlbedo.instance.getModules().handleConfig(configuration);
        if (configuration.hasChanged()){
            configuration.save();
        }
    }

}
