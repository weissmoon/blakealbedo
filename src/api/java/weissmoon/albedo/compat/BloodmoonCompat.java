package weissmoon.albedo.compat;

import net.minecraftforge.fml.common.event.*;

/**
 * Created by Weissmoon on 6/12/18.
 */
public class BloodmoonCompat implements CompatModule {
    @Override
    public void preInit(FMLPreInitializationEvent event) {

    }

    @Override
    public void init(FMLInitializationEvent event) {

    }

    @Override
    public void postInit(FMLPostInitializationEvent event) {

    }

    @Override
    public void serverAbouttoStart(FMLServerAboutToStartEvent evt) {

    }

    @Override
    public void serverStarting(FMLServerStartingEvent evt) {

    }

    @Override
    public void serverStarted(FMLServerStartedEvent evt) {

    }

    @Override
    public void serverAbouttoStop(FMLServerStoppingEvent evt) {

    }

    @Override
    public void serverStoped(FMLServerStartingEvent evt) {

    }

    @Override
    public String getModName() {
        return null;
    }
}
