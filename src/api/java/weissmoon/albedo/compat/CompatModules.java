package weissmoon.albedo.compat;

import net.minecraftforge.fml.common.Loader;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.event.FMLStateEvent;

import java.util.HashMap;
import java.util.HashSet;

/**
 * Created by Weissmoon on 1/2/18.
 */
public class CompatModules {

    private HardcoreDarknessCompat hardcoreDarknessCompat;
    private HashMap<String, CompatModule> map = new HashMap<>(1);

    public void setupModules(){
        if(Loader.isModLoaded("hardcoredarkness")){
            hardcoreDarknessCompat = new HardcoreDarknessCompat();
            map.put("hardcoredarkness", hardcoreDarknessCompat);
        }
    }

    public void stateevent(FMLStateEvent event){
        if(event.getClass().isInstance(FMLPreInitializationEvent.class)) {
            try {
                map.get("hardcoredarkness").preInit((FMLPreInitializationEvent)event);
            } catch (Exception e) {

            }
        }
    }

    public CompatModule getModel(String name){
        if(map.containsKey(name))
            return map.get(name);
        return null;
    }

    public boolean isLoaded(String name){
        return map.containsKey(name);
    }
}
