package weissmoon.albedo.server.events;

import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.PlayerEvent;
import net.minecraftforge.fml.common.network.NetworkRegistry;
import weissmoon.albedo.server.ConfigurationHandlerServer;
import weissmoon.albedo.server.network.NetworkHandler;
import weissmoon.albedo.server.network.PlayerPermisionPacket;

/**
 * Created by Weissmoon on 1/15/18.
 */
public class PlayerConnected {

    public static final PlayerConnected INSTANCE = new PlayerConnected();

    private static PlayerPermisionPacket packet = new PlayerPermisionPacket();

    @SubscribeEvent
    public final void onPlayerJoinWorldEvent(PlayerEvent.PlayerLoggedInEvent event){
        EntityPlayerMP player = (EntityPlayerMP) event.player;
        if(player.connection.getNetworkManager().isLocalChannel())
            return;
        Boolean hasForge = player.connection.getNetworkManager().channel().attr(NetworkRegistry.FML_MARKER).get();
        if(hasForge){
            setupPackage();
            NetworkHandler.INSTANCE.sendTo(packet, player);
        }
    }

    private static void setupPackage(){
        packet.players = ConfigurationHandlerServer.playerHeldServer;
        packet.items = ConfigurationHandlerServer.itemEntityServer;
        packet.world = ConfigurationHandlerServer.worldLightServer;
    }
}
