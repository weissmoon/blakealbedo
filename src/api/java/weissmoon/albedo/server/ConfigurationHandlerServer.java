package weissmoon.albedo.server;

import net.minecraftforge.common.config.Configuration;

import java.io.File;

/**
 * Created by Weissmoon on 12/2/17.
 */
public class ConfigurationHandlerServer {
    public static Configuration configuration;

    public static boolean itemEntityServer;
    public static boolean playerHeldServer;
    //public static boolean itemListAlso;

    public static boolean worldLightServer;

    public static String CATEGORY_ENTITY = "server";

    public static void init (File configFile){
        if (configuration == null){
            configuration = new Configuration(configFile);
        }
        loadCofigurationServer();
    }

    protected static void loadCofigurationServer (){
        itemEntityServer = configuration.getBoolean("itemEntity", CATEGORY_ENTITY, true, "Items Light Up");
        //itemListAlso = configuration.getBoolean("itemListAlso", CATEGORY_ENTITY, true, "Entity and Player Held Items are the same list");

        playerHeldServer = configuration.getBoolean("playerHeld", CATEGORY_ENTITY, true, "Light Player");

        worldLightServer = configuration.getBoolean("worldLight", CATEGORY_ENTITY, true, "\"Fix\" Albedo Darkness");

        if (configuration.hasChanged()){
            configuration.save();
        }
    }
}
