package weissmoon.albedo.server.network;

import net.minecraftforge.fml.common.network.NetworkRegistry;
import net.minecraftforge.fml.common.network.simpleimpl.SimpleNetworkWrapper;
import net.minecraftforge.fml.relauncher.Side;
import weissmoon.albedo.Reference;

/**
 * Created by Weissmoon on 1/15/18.
 */
public class NetworkHandler {

    public static final SimpleNetworkWrapper INSTANCE = NetworkRegistry.INSTANCE.newSimpleChannel(Reference.MOD_ID);

    public static void init () {
        INSTANCE.registerMessage(PlayerPermisionPacket.Handler.class, PlayerPermisionPacket.class, 0, Side.CLIENT);
    }
}
