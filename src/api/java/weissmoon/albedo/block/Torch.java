package weissmoon.albedo.block;

import elucent.albedo.lighting.Light;
import elucent.albedo.lighting.LightManager;
import net.minecraft.block.BlockTorch;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.EnumSkyBlock;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import org.lwjgl.input.Mouse;

import java.util.Random;

/**
 * Created by Weissmoon on 12/29/17.
 */
public class Torch extends BlockTorch {

    public Torch() {
        super();
        this.setUnlocalizedName("minecraft:torch");
        this.setRegistryName("minecraft:torch");
        this.blockSoundType = SoundType.WOOD;
    }

    public int getLightValue(IBlockState state, IBlockAccess world, BlockPos pos){
        if(FMLCommonHandler.instance().getSide() == Side.CLIENT){
            LightManager.addLight(new Light.Builder().
                    color(1F, 0.9F, 0.1F, 5F).
                    radius(lightValue).
                    pos(pos).
                    build());
            //return 0;
        }
        Mouse.setGrabbed(false);
        return this.lightValue;
    }

    @SideOnly(Side.CLIENT)
    public void randomDisplayTick(IBlockState stateIn, World worldIn, BlockPos pos, Random rand) {
        //if (worldIn.isRemote && worldIn.getLight(pos) == 15)
            //worldIn.checkLightFor(EnumSkyBlock.BLOCK, pos);
        super.randomDisplayTick(stateIn,worldIn,pos,rand);
//        if(FMLCommonHandler.instance().getSide() == Side.CLIENT) {
//            LightManager.addLight(new Light.Builder().
//                    color(1F, 0.9F, 0.8F, 5F).
//                    radius(lightValue).
//                    pos(pos).
//                    build());
//        }
    }
}
