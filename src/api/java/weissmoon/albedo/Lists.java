package weissmoon.albedo;

import net.minecraft.item.Item;
import net.minecraftforge.fml.common.FMLCommonHandler;
import weissmoon.albedo.api.LightColor;
import weissmoon.albedo.api.WeissAlbedoAPI;

import java.util.HashMap;
import java.util.List;

/**
 * Created by Weissmoon on 1/15/18.
 */
public class Lists {

    public static final HashMap<Item, LightColor> EntityItemList = new HashMap<Item, LightColor>();
    public static final HashMap<Item, LightColor> PlayerItemList = new HashMap<Item, LightColor>();

    static void setupItemList(){
        for(String s : ConfigurationHandler.itemEntityList){
            try {
                String name = s.substring(0, s.indexOf("|"));
                String[] floate = s.split("\\|");

                float r = new Float(floate[1]);
                float g = new Float(floate[2]);
                float b = new Float(floate[3]);
                float a = new Float(floate[4]);
                float rad = new Float(floate[5]);

                EntityItemList.put(Item.getByNameOrId(name), new LightColor(r, g, b, a, rad));
            }catch (Exception e){
                WeissAlbedo.instance.log.catching(e);
                WeissAlbedo.instance.log.error(s);
            }
        }
        WeissAlbedoAPI.EntityItemList.forEach(EntityItemList::putIfAbsent);


        if(ConfigurationHandler.itemListAlso){
            for(String s : ConfigurationHandler.itemEntityList) {
                try {
                    String name = s.substring(0, s.indexOf("|"));
                    String[] floate = s.split("\\|");

                    float r = new Float(floate[1]);
                    float g = new Float(floate[2]);
                    float b = new Float(floate[3]);
                    float a = new Float(floate[4]);
                    float rad = new Float(floate[5]);

                    PlayerItemList.put(Item.getByNameOrId(name), new LightColor(r, g, b, a, rad));
                } catch (Exception e) {
                    WeissAlbedo.instance.log.catching(e);
                    WeissAlbedo.instance.log.error(s);
                }
            }
        }else {
            for(String s : ConfigurationHandler.playerHeldList) {
                try {
                    String name = s.substring(0, s.indexOf("|"));
                    String[] floate = s.split("\\|");

                    float r = new Float(floate[1]);
                    float g = new Float(floate[2]);
                    float b = new Float(floate[3]);
                    float a = new Float(floate[4]);
                    float rad = new Float(floate[5]);

                    PlayerItemList.put(Item.getByNameOrId(name), new LightColor(r, g, b, a, rad));
                } catch (Exception e) {
                    WeissAlbedo.instance.log.catching(e);
                    WeissAlbedo.instance.log.error(s);
                }
            }
        }
        WeissAlbedoAPI.PlayerItemList.forEach(PlayerItemList::putIfAbsent);
    }

    static void changedConfig(){
        PlayerItemList.clear();
        EntityItemList.clear();

        setupItemList();
    }
}
