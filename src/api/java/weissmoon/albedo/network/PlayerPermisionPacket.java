package weissmoon.albedo.network;

import io.netty.buffer.ByteBuf;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;
import weissmoon.albedo.PermissionHandler;

/**
 * Created by Weissmoon on 2/14/18.
 */
public class PlayerPermisionPacket implements IMessage {

    public boolean items;
    public boolean players;
    public boolean world;

    protected static int os;

    @Override
    public void fromBytes(ByteBuf buf) {
        players = buf.readBoolean();
        items = buf.readBoolean();
        world = buf.readBoolean();
    }

    @Override
    public void toBytes(ByteBuf buf) {
        buf.writeBoolean(players);
        buf.writeBoolean(items);
        buf.writeBoolean(world);
    }

    public static class Handler implements IMessageHandler<PlayerPermisionPacket, IMessage> {

        @Override
        public IMessage onMessage(PlayerPermisionPacket message, MessageContext ctx) {
            PermissionHandler.handleServerPermisionPacket(message);
            return null;
        }
    }

    public int getOS(){
        return os;
    }
}
