package weissmoon.albedo.api;

import elucent.albedo.lighting.Light;
import net.minecraft.entity.Entity;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumHand;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

/**
 * Created by Weissmoon on 2/5/18.
 * Used in items to provide a light without building a proper light
 */
public interface IItemLightProvider{

    /**
     *
     * @param stack is the stack the item is in.
     * @param entity the entity holding the stack.
     * @param hand the hand of the entity if used.
     * @return color of light to add to world needs r,g,b,a,radius
     */
    @SideOnly(Side.CLIENT)
    public LightColor provideLight(ItemStack stack, Entity entity, EnumHand hand);
}
