package weissmoon.albedo.api;

import elucent.albedo.lighting.Light;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraftforge.oredict.OreDictionary;

import javax.annotation.Nonnull;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Weissmoon on 1/15/18.
 */
public class WeissAlbedoAPI {

    public static final HashMap<Item, LightColor> EntityItemList = new HashMap<Item, LightColor>();
    public static final HashMap<Item, LightColor> PlayerItemList = new HashMap<Item, LightColor>();

    public static final HashMap<Item, IItemLightProvider> EntityItemProvider = new HashMap<Item, IItemLightProvider>();
    public static final HashMap<Item, IItemLightProvider> PlayerItemProvider = new HashMap<Item, IItemLightProvider>();

    public void addToEntityItemList(@Nonnull ItemStack itemStack, LightColor light){
        EntityItemList.put(itemStack.getItem(), light);
    }

    public void addToPlayerItemList(@Nonnull ItemStack itemStack, LightColor light){
        PlayerItemList.put(itemStack.getItem(), light);
    }

    public void addToEntityItemProvider(@Nonnull ItemStack itemStack, IItemLightProvider light){
        EntityItemProvider.put(itemStack.getItem(), light);
    }

    public void addToPlayerItemProvider(@Nonnull ItemStack itemStack, IItemLightProvider light){
        PlayerItemProvider.put(itemStack.getItem(), light);
    }
}
