package weissmoon.albedo.tile;

import elucent.albedo.event.RenderTileEntityEvent;
import elucent.albedo.lighting.Light;
import elucent.albedo.lighting.LightManager;
import net.minecraft.item.EnumDyeColor;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.tileentity.TileEntityBeacon;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

import java.util.List;

/**
 * Created by Weissmoon on 11/27/17.
 */
@Mod.EventBusSubscriber
public class BeaconColoringLight{

    @SubscribeEvent
    public void render(RenderTileEntityEvent evnt){
        TileEntity tile = evnt.getEntity();
        if (tile instanceof TileEntityBeacon) {
            if(((TileEntityBeacon) evnt.getEntity()).getLevels() > 0) {
                float r,g,b, h;
                List<TileEntityBeacon.BeamSegment> segs = ((TileEntityBeacon) tile).getBeamSegments();
                if (segs.size() == 0)return;
                TileEntityBeacon.BeamSegment se;
                if (segs.size() > 1) {
                    se = ((TileEntityBeacon) tile).getBeamSegments().get(0);
                }else {
                    return;
                }
                h = 15 - se.getHeight();
                if(h < 1) return;
                h = h < 0 ? 0 : h;
                se = ((TileEntityBeacon) tile).getBeamSegments().get(1);
                float[] c = se.getColors();
                c = c == null ? EnumDyeColor.WHITE.getColorComponentValues(): c;
                r = c[0]*2F;
                g = c[1]*2F;
                b = c[2]*2F;
                LightManager.addLight(new Light.Builder().pos(tile.getPos()).color( r, g, b, 2F).radius(h).build());
            }
        }
    }
}
