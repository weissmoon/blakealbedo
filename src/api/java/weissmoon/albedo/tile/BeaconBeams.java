package weissmoon.albedo.tile;

import elucent.albedo.event.RenderTileEntityEvent;
import elucent.albedo.lighting.Light;
import elucent.albedo.lighting.LightManager;
import net.minecraft.item.EnumDyeColor;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.tileentity.TileEntityBeacon;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import weissmoon.albedo.ConfigurationHandler;

import java.util.List;

/**
 * Created by Weissmoon on 12/2/17.
 */
public class BeaconBeams {

    @SubscribeEvent
    public void renderBeams(RenderTileEntityEvent evnt) {
        TileEntity tile = evnt.getEntity();
        if (tile instanceof TileEntityBeacon) {
            if (((TileEntityBeacon) evnt.getEntity()).getLevels() > 0) {
                float r, g, b, h;
                List<TileEntityBeacon.BeamSegment> segs = ((TileEntityBeacon) tile).getBeamSegments();
                if (segs.size() == 0) return;
                int count = 0;
                h = 1;
                segments:for (TileEntityBeacon.BeamSegment s : segs) {
                    TileEntityBeacon.BeamSegment se = ((TileEntityBeacon) tile).getBeamSegments().get(count);
                    count++;
                    if(count == 1 && ConfigurationHandler.beacon){
                        h += se.getHeight();
                        continue segments;
                    }
                    float[] c = se.getColors();
                    c = c == null ? EnumDyeColor.WHITE.getColorComponentValues(): c;
                    r = c[0];
                    g = c[1];
                    b = c[2];
                    LightManager.addLight(new Light.Builder().pos(tile.getPos().add(0, h, 0)).color( r, g, b, 1F).radius(20).build());
                    h += se.getHeight();
                }
            }
        }
    }
}
