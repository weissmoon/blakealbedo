package weissmoon.albedo.entity;

import elucent.albedo.event.RenderEntityEvent;
import elucent.albedo.lighting.Light;
import elucent.albedo.lighting.LightManager;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumHand;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import weissmoon.albedo.Lists;
import weissmoon.albedo.api.IItemLightProvider;
import weissmoon.albedo.api.LightColor;
import weissmoon.albedo.api.WeissAlbedoAPI;

/**
 * Created by Weissmoon on 1/15/18.
 */
public class ItemEntityLight {

    @SubscribeEvent
    public void renderItemEntity(RenderEntityEvent evnt){
        if (evnt.getEntity() instanceof EntityItem){
            EntityItem player = (EntityItem) evnt.getEntity();
            if(!player.isInvisibleToPlayer(Minecraft.getMinecraft().player)){
                ItemStack stack = player.getItem();
                if(stack.getItem() instanceof IItemLightProvider){
                    LightManager.addLight(LightColor.builder(
                            ((IItemLightProvider) stack.getItem()).provideLight(stack, player, null)).
                            pos(player.getEntityBoundingBox().getCenter()).
                            build());
                }else if(WeissAlbedoAPI.EntityItemProvider.containsKey(stack.getItem())){
                    IItemLightProvider provider = WeissAlbedoAPI.EntityItemProvider.get(stack.getItem());

                    LightManager.addLight(LightColor.builder(
                            provider.provideLight(stack, player, null)).
                            pos(player.getEntityBoundingBox().getCenter()).
                            build());
                }else if(light(player.getItem())) {
                    LightManager.addLight(LightColor.builder(Lists.EntityItemList.get(player.getItem().getItem())).
                            pos(player.getEntityBoundingBox().getCenter()).
                            build());
//                        LightManager.addLight(new Light.Builder().
//                                pos(player.getEntityBoundingBox().getCenter()).
//                                color(1F, 1F, 1F).
//                                radius(5F).
//                                build());
                }
            }
        }
    }


    private boolean light(ItemStack stack){
        for(Item item: Lists.EntityItemList.keySet()){
            ItemStack stack1 = new ItemStack(item);
            if(stack1.isItemEqualIgnoreDurability(stack)){
                return true;
            }else if(stack1.isItemEqual(stack)){
                return true;
            }
        }
        return false;
    }

}
