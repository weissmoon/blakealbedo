package weissmoon.albedo.entity;

import elucent.albedo.event.RenderEntityEvent;
import elucent.albedo.lighting.Light;
import elucent.albedo.lighting.LightManager;
import net.minecraft.entity.Entity;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

/**
 * Created by Weissmoon on 12/20/17.
 */
@Mod.EventBusSubscriber
public class EntityBurning {

    @SubscribeEvent
    public void renderBurning(RenderEntityEvent evnt){
        Entity entity = evnt.getEntity();
        if(entity instanceof Entity){
            if(entity.isBurning() && entity.canRenderOnFire()){
                LightManager.addLight(new Light.Builder()
                        .pos(entity.getEntityBoundingBox().getCenter())
                        .color(0.8f, 0.4f, 0f, 2F)
                        .radius((float)(entity.getRenderBoundingBox().getAverageEdgeLength() * 10))
                        .build());
            }
        }
    }
}
