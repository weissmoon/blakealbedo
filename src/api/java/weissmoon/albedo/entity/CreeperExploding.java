package weissmoon.albedo.entity;

import elucent.albedo.event.RenderEntityEvent;
import elucent.albedo.lighting.Light;
import elucent.albedo.lighting.LightManager;
import net.minecraft.entity.Entity;
import net.minecraft.entity.monster.EntityCreeper;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

/**
 * Created by Weissmoon on 12/2/17.
 */
public class CreeperExploding {

    @SubscribeEvent
    public void renderCreeperExplosion(RenderEntityEvent evnt) {
        Entity entity = evnt.getEntity();
        if(entity instanceof EntityCreeper) {
            if(((EntityCreeper) entity).getCreeperState() == 1) {
                //Vazkii math
                float flash = 1F - (((EntityCreeper) entity).getCreeperFlashIntensity(0) / 1.17F + 0.1F);
                NBTTagCompound nbt = new NBTTagCompound();
                ((EntityCreeper) entity).writeEntityToNBT(nbt);
                float explosionRadius = nbt.getByte("ExplosionRadius");
                float lightRadius = explosionRadius - (explosionRadius * flash);

                LightManager.addLight(new Light.Builder()
                        .pos(entity.getEntityBoundingBox().getCenter())
                        .color(1.5f, 0.8f, 0f, 1.5F)
                        .radius(lightRadius *2)
                        .build());
                return;
            }
        }
    }
}
