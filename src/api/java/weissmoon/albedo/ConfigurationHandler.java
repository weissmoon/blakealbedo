package weissmoon.albedo;

import net.minecraftforge.common.config.Configuration;
import net.minecraftforge.fml.client.event.ConfigChangedEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import weissmoon.albedo.server.ConfigurationHandlerServer;

import java.io.File;
import java.util.regex.Pattern;

/**
 * Created by Weissmoon on 12/2/17.
 */
public class ConfigurationHandler extends ConfigurationHandlerServer {
    //public static Configuration configuration;

    public static boolean beacon;
    public static boolean beaconSegments;
    public static boolean furnace;

    public static boolean creeperCharged;
    public static boolean creeperExplosion;
    public static boolean burningEntities;
    public static boolean itemEntity;
    public static boolean playerHeld;
    public static boolean itemListAlso;

    public static boolean worldLight;
    public static float worldLightValue;

    public static String[] itemEntityList;
    public static String[] playerHeldList;

    public static String CATEGORY_TILES = "tiles";
    public static String CATEGORY_ENTITY = "entity";
    public static String CATEGORY_WORLD = "world";
    public static String CATEGORY_CHEATS = "other";

    Pattern commaDelimitedPattern = Pattern.compile("([A-Za-z]+((,){1}( )*|$))+?");

    public static void initC (File configFile){
        if (configuration == null){
            configuration = new Configuration(configFile);
            loadCofiguration();
        }
        //configureServer();
        //configuration.setCategoryRequiresWorldRestart(CATEGORY_CHEATS, true);
    }

    @SubscribeEvent
    public void onConfigurationChangedEvent(ConfigChangedEvent.OnConfigChangedEvent event){
        if (event.getModID().equals(Reference.MOD_ID)){
            loadCofiguration();
        }
    }

    private static void loadCofiguration (){
        beacon = configuration.getBoolean("bacon", CATEGORY_TILES, true, "Beacon");
        beaconSegments = configuration.getBoolean("baconSegments", CATEGORY_TILES, true, "Beacon Segments");
        furnace = configuration.getBoolean("furnace", CATEGORY_TILES, true, "Furnace");

        creeperCharged = configuration.getBoolean("creeperCharged", CATEGORY_ENTITY, true, "Charged Creeper");
        creeperExplosion = configuration.getBoolean("creeperExplosion", CATEGORY_ENTITY, true, "Creeper Explosion");
        burningEntities = configuration.getBoolean("burningEntities", CATEGORY_ENTITY, true, "Burning Entities");

        worldLight = configuration.getBoolean("worldLight", CATEGORY_CHEATS, false, "\"Fix\" Albedo Darkness");
        worldLightValue = configuration.getFloat("worldLightValue", CATEGORY_WORLD, 0.1F, 0F, 0.5F, "World Light Level");

        itemEntity = configuration.getBoolean("itemEntity", CATEGORY_CHEATS, true, "Items Light Up");
        itemEntityList = configuration.getStringList("itemEntity list", CATEGORY_ENTITY,
                new String[]{"minecraft:torch|1|1|1|1|5",
                        "minecraft:glowstone|0.82|0.82|0|0.8|5",
                        "minecraft:lit_pumpkin|0.82|0.82|0|0.8|5",
                        "minecraft:sea_lantern|0.35|0.51|0.47|1.2|5",
                        "minecraft:magma|0.88|0.39|0.06|0.6|3",
                        "minecraft:lava_bucket|0.88|0.39|0.06|1.2|5"
                },
                "Entity Items");
        itemListAlso = configuration.getBoolean("itemListAlso", CATEGORY_ENTITY, true, "Entity and Player Held Items are the same list");

        playerHeld = configuration.getBoolean("playerHeld", CATEGORY_CHEATS, true, "Light Player");
        if(!itemListAlso)
        playerHeldList = configuration.getStringList("playerHeld list", CATEGORY_ENTITY,
                new String[]{"minecraft:torch|1|1|1|1|7",
                        "minecraft:glowstone|0.82|0.82|0|0.8|7",
                        "minecraft:lit_pumpkin|0.82|0.82|0|0.8|7",
                        "minecraft:sea_lantern|0.35|0.51|0.47|1.2|7",
                        "minecraft:magma|0.88|0.39|0.06|0.6|5",
                        "minecraft:lava_bucket|0.88|0.39|0.06|1.2|7"
                },
                "Player Items");

        ConfigurationHandlerServer.loadCofigurationServer();
        //if (configuration.hasChanged()){
            configuration.save();
            Lists.changedConfig();
            WeissAlbedo.instance.reregisterObjects();
            //configureServer();
            PlayerConnectedClient.configurationchanged();
        //}
    }

//    private static void configureServer(){
//        ConfigurationHandlerServer.playerHeld = playerHeld;
//        ConfigurationHandlerServer.itemEntity = itemEntity;
//        ConfigurationHandlerServer.worldLight = worldLight;
//    }
}
