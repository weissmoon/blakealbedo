package weissmoon.albedo.gui;

import net.minecraft.client.gui.GuiScreen;
import net.minecraftforge.common.config.ConfigElement;
import net.minecraftforge.fml.client.config.ConfigGuiType;
import net.minecraftforge.fml.client.config.DummyConfigElement;
import net.minecraftforge.fml.client.config.GuiConfig;
import net.minecraftforge.fml.client.config.IConfigElement;
import weissmoon.albedo.ConfigurationHandler;
import weissmoon.albedo.Reference;
import weissmoon.albedo.server.ConfigurationHandlerServer;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

public class ModGuiConfig extends GuiConfig{
    public ModGuiConfig (GuiScreen guiScreen){
        super(guiScreen, getConfigElements(),
                //new ConfigElement(ConfigurationHandler.configuration.getCategory("general")).getChildElements(),
                Reference.MOD_ID,
                false,
                false,
                GuiConfig.getAbridgedConfigPath(ConfigurationHandler.configuration.toString()));
    }


    public static List<IConfigElement> getConfigElements(){
        List<IConfigElement> list = new ArrayList<>();

        list.add(new ConfigElement(ConfigurationHandler.configuration.getCategory(ConfigurationHandler.CATEGORY_ENTITY)));
        list.add(new ConfigElement(ConfigurationHandler.configuration.getCategory(ConfigurationHandler.CATEGORY_TILES)));
        list.add(new ConfigElement(ConfigurationHandler.configuration.getCategory(ConfigurationHandler.CATEGORY_WORLD)));
        list.add(new ConfigElement(ConfigurationHandler.configuration.getCategory(ConfigurationHandler.CATEGORY_CHEATS)));
        list.add(new ConfigElement(ConfigurationHandler.configuration.getCategory(ConfigurationHandlerServer.CATEGORY_ENTITY)));

        return list;
    }
}
