package weissmoon.albedo;

import net.minecraft.client.Minecraft;
import weissmoon.albedo.network.PlayerPermisionPacket;
import weissmoon.albedo.server.proxy.ServerProxy;

/**
 * Created by Weissmoon on 2/14/18.
 */
public class ClientProxy extends ServerProxy{

    @Override
    public boolean isSinglePlayer(){
        return Minecraft.getMinecraft().isIntegratedServerRunning();
    }
}
