package weissmoon.albedo.world;

import elucent.albedo.event.ProfilerStartEvent;
import elucent.albedo.lighting.LightManager;
import net.minecraft.client.Minecraft;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import weissmoon.albedo.ConfigurationHandler;
import weissmoon.albedo.api.LightColor;

/**
 * Created by Weissmoon on 2/10/18.
 */
public class WorldLight extends WorldLightUpper {

    //Dumb fix
    private static LightColor natural;

    public WorldLight init(){
        float worldLightValue = ConfigurationHandler.worldLightValue;
        natural = new LightColor(1, 1, 1, worldLightValue, 500);
        return this;
    }

    @SubscribeEvent
    public void onProfileStart(ProfilerStartEvent evnt) {
        if (evnt.getSection().compareTo("terrain") == 0) {
            World world = Minecraft.getMinecraft().world;
            if (world == null) return;
            if (getDarkness()) return;
            LightManager.addLight(LightColor.builder(natural).pos(Minecraft.getMinecraft().player.getPosition()).build());
        }
    }

}
