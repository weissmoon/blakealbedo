package weissmoon.albedo.world;

import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.PlayerEvent;
import weissmoon.albedo.WeissAlbedo;
import weissmoon.albedo.compat.HardcoreDarknessCompat;

/**
 * Created by Weissmoon on 3/3/18.
 */
public class WorldLightUpper {
    private static boolean darkness;

    @SubscribeEvent
    public void onDimensionChange(PlayerEvent.PlayerChangedDimensionEvent event){
        if(WeissAlbedo.instance.getModules().isLoaded("hardcoredarkness"))
        darkness = !((HardcoreDarknessCompat) WeissAlbedo.instance.getModules().getModel("hardcoredarkness")).isDimensionBlacklist(event.toDim);
    }

    protected final boolean getDarkness(){
        return darkness;
    }
}
