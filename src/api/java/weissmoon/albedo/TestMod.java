package weissmoon.albedo;

import net.minecraft.block.Block;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.*;
import net.minecraft.entity.monster.*;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;
import net.minecraft.tileentity.MobSpawnerBaseLogic;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.tileentity.TileEntityMobSpawner;
import net.minecraft.util.EnumActionResult;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.world.World;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.registries.IForgeRegistry;

import javax.annotation.Nullable;
import java.util.List;

/**
 * Created by Weissmoon on 3/2/18.
 */
//@Mod(modid = "odiaj")
public class TestMod {

    @Mod.EventHandler
    public void pre(FMLPreInitializationEvent event){
        MinecraftForge.EVENT_BUS.register(this);
    }
    @SubscribeEvent
    public void registerItems(RegistryEvent.Register<Item> evt) {
        IForgeRegistry<Item> GameRegistry = evt.getRegistry();

        GameRegistry.register(new Lighting());
    }
    public class Lighting extends Item{

        public Lighting() {
            this.setRegistryName("odiaj", "odiso");
        }

        public EnumActionResult onItemUse(EntityPlayer player, World worldIn, BlockPos pos, EnumHand hand, EnumFacing facing, float hitX, float hitY, float hitZ) {
            ItemStack itemstack = player.getHeldItem(hand);

            if (worldIn.isRemote) {
                return EnumActionResult.SUCCESS;
            } else if (!player.canPlayerEdit(pos.offset(facing), facing, itemstack)) {
                return EnumActionResult.FAIL;
            } else {
                IBlockState iblockstate = worldIn.getBlockState(pos);
                Block block = iblockstate.getBlock();

                BlockPos blockpos = pos.offset(facing);
                double d0 = this.getYOffset(worldIn, blockpos);

                Entity entity = spawnCreature(worldIn, getNamedIdFrom(itemstack), (double) blockpos.getX() + 0.5D, (double) blockpos.getY() + d0, (double) blockpos.getZ() + 0.5D);

                entity.setRotationYawHead((float) (Math.PI) / 2f);

                if (entity != null) {
                    if (entity instanceof EntityLivingBase && itemstack.hasDisplayName()) {
                        entity.setCustomNameTag(itemstack.getDisplayName());
                    }

                    //applyItemEntityDataToEntity(worldIn, player, itemstack, entity);

                    if (!player.capabilities.isCreativeMode) {
                        //itemstack.shrink(1);
                    }
                }

                return EnumActionResult.SUCCESS;

            }
        }

        protected double getYOffset(World p_190909_1_, BlockPos p_190909_2_)
        {
            AxisAlignedBB axisalignedbb = (new AxisAlignedBB(p_190909_2_)).expand(0.0D, -1.0D, 0.0D);
            List<AxisAlignedBB> list = p_190909_1_.getCollisionBoxes((Entity)null, axisalignedbb);

            if (list.isEmpty())
            {
                return 0.0D;
            }
            else
            {
                double d0 = axisalignedbb.minY;

                for (AxisAlignedBB axisalignedbb1 : list)
                {
                    d0 = Math.max(axisalignedbb1.maxY, d0);
                }

                return d0 - (double)p_190909_2_.getY();
            }
        }

        public Entity spawnCreature(World worldIn, @Nullable ResourceLocation entityID, double x, double y, double z) {
            EntityLiving entitye = new EntityGiantZombie(worldIn);
            entitye.addPotionEffect(new PotionEffect(Potion.getPotionById(12), 20000, 2, false, false));
            //entitye.setFire(5000);
            //creeper.onStruckByLightning(null);
                Entity entity = null;

                for (int i = 0; i < 1; ++i) {
                    entity = entitye;

                    if (entity instanceof EntityLiving) {
                        EntityLiving entityliving = (EntityLiving) entity;
                        entity.setLocationAndAngles(x, y, z, MathHelper.wrapDegrees(worldIn.rand.nextFloat() * 360.0F), 0.0F);
                        entityliving.rotationYawHead = entityliving.rotationYaw;
                        entityliving.renderYawOffset = entityliving.rotationYaw;
                        entityliving.onInitialSpawn(worldIn.getDifficultyForLocation(new BlockPos(entityliving)), (IEntityLivingData) null);
                        worldIn.spawnEntity(entity);
                        entityliving.playLivingSound();
                    }
                }

                return entity;

        }

        public ResourceLocation getNamedIdFrom(ItemStack stack) {
            NBTTagCompound nbttagcompound = stack.getTagCompound();

            if (nbttagcompound == null) {
                return null;
            } else if (!nbttagcompound.hasKey("EntityTag", 10)) {
                return null;
            } else {
                NBTTagCompound nbttagcompound1 = nbttagcompound.getCompoundTag("EntityTag");

                if (!nbttagcompound1.hasKey("id", 8)) {
                    return null;
                } else {
                    String s = nbttagcompound1.getString("id");
                    ResourceLocation resourcelocation = new ResourceLocation(s);

                    if (!s.contains(":")) {
                        nbttagcompound1.setString("id", resourcelocation.toString());
                    }

                    return resourcelocation;
                }
            }
        }
    }
}
