package weissmoon.albedo;

import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.PlayerEvent;
import net.minecraftforge.fml.common.network.NetworkRegistry;
import weissmoon.albedo.network.ClientNetworkHandler;
import weissmoon.albedo.network.PlayerPermisionPacket;
import weissmoon.albedo.server.ConfigurationHandlerServer;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Weissmoon on 2/18/18.
 */
public class PlayerConnectedClient  {

    private static final PlayerConnectedClient INSTANCE = new PlayerConnectedClient();

    private static PlayerPermisionPacket packet = new PlayerPermisionPacket();

    private static boolean[] pso = new boolean[4];

    private static List<EntityPlayerMP> forgePlayers = new ArrayList<EntityPlayerMP>(1);

    @SubscribeEvent
    public final void onPlayerJoinWorldEvent(PlayerEvent.PlayerLoggedInEvent event) {
        EntityPlayerMP player = (EntityPlayerMP) event.player;
        Boolean hasForge = player.connection.getNetworkManager().channel().attr(NetworkRegistry.FML_MARKER).get();
        if (hasForge) {
            INSTANCE.setupPackage();
            forgePlayers.add(player);
            ClientNetworkHandler.INSTANCE.sendTo(INSTANCE.packet, player);
        }
    }

    @SubscribeEvent
    public final void onPlayerLeaveWorldEvent(PlayerEvent.PlayerLoggedOutEvent event){
        try {
            EntityPlayerMP player = (EntityPlayerMP) event.player;
            Boolean hasForge = player.connection.getNetworkManager().channel().attr(NetworkRegistry.FML_MARKER).get();
            if (hasForge) {
                forgePlayers.remove(player);
            }
        }catch(Exception e){
            WeissAlbedo.instance.log.error(e);
        }
    }

    static final void configurationchanged(){
        if(pso[3]) {
            INSTANCE.setupPackage();
            //ClientNetworkHandler.INSTANCE.sendToAll(PlayerConnected.packet);
            for (EntityPlayerMP p : forgePlayers) {
                ClientNetworkHandler.INSTANCE.sendTo(INSTANCE.packet, p);
            }
            WeissAlbedo.instance.stopCheatEvents();
        }
        WeissAlbedo.instance.handelConfigChange();
    }

    private static void setupPackage(){
        packet.players = ConfigurationHandlerServer.playerHeldServer;
        packet.items = ConfigurationHandlerServer.itemEntityServer;
        packet.world = ConfigurationHandlerServer.worldLightServer;
    }
    static void prosen(boolean[] ss){
        pso = ss;
    }
}
