package weissmoon.albedo;

/**
 * Created by Weissmoon on 11/27/17.
 */
public class Reference {
    public static final String MOD_ID = "weissalbedo";
    public static final String MOD_NAME = "Weiss Albedo";
    public static final String VERSION = "0.0.14";
    public static final String AUTHOR = "Weissmoon";
    public static final String GUI_FACTORY_CLASS = "weissmoon.albedo.gui.GuiFactory";
    public static final String DESCRIPTION = "Vanilla Albedo.";
}
