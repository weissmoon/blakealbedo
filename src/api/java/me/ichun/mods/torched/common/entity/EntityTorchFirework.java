package me.ichun.mods.torched.common.entity;

import net.minecraft.entity.Entity;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.datasync.DataParameter;
import net.minecraft.network.datasync.DataSerializers;
import net.minecraft.network.datasync.EntityDataManager;
import net.minecraft.world.World;

/**
 * Created by Weissmoon on 2/10/19.
 */
public class EntityTorchFirework extends Entity {
    private static final DataParameter<Integer> TORCHES = EntityDataManager.createKey(EntityTorchFirework.class, DataSerializers.VARINT);
    public EntityTorchFirework(World worldIn) {
        super(worldIn);
    }

    @Override
    protected void entityInit() {

    }

    @Override
    protected void readEntityFromNBT(NBTTagCompound compound) {

    }

    @Override
    protected void writeEntityToNBT(NBTTagCompound compound) {

    }
    public int getTorches(){
        return 0;
    }

    public boolean getRocket()
    {
        return true;
    }

    public int getGP(){
        return 0;
    }
}
