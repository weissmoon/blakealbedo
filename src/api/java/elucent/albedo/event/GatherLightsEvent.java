package elucent.albedo.event;

import elucent.albedo.lighting.Light;
import net.minecraftforge.fml.common.eventhandler.Event;

import java.util.ArrayList;

public class GatherLightsEvent extends Event {
	ArrayList<Light> lights = new ArrayList<Light>();
	public GatherLightsEvent(ArrayList<Light> lights){
		super();
		this.lights = lights;
	}
	
	public ArrayList<Light> getLightList(){
		return lights;
	}
	
	@Override
	public boolean isCancelable(){
		return false;
	}
}