package elucent.albedo.item;

import net.minecraft.item.Item;

import java.util.HashMap;
import java.util.Map;

public class ItemRenderRegistry {
	public static Map<Item, IItemSpecialRenderer> itemRenderMap = new HashMap<Item, IItemSpecialRenderer>();

	public static void register(Item i, IItemSpecialRenderer r){
		itemRenderMap.put(i, r);
	}
}
